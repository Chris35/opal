# Facilitating the Static Analysis Development
OPAL comes with a large number of tools to analyze and visualize various aspects related to Bytecode. These tools can be used as is or can be embedded into other tools. Several of the tools require Graphviz to visualize things like control- and data-flow information.

To use the tools, start `sbt`, change to the `project OPAL-DeveloperTools` project and call `run`. Afterwards, you'll which tools are available.
